import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Navigation',
    main: [
      {
        state: 'partner-organization-list',
        short_label: 'D',
        name: 'Partner Organization List',
        type: 'link',
        icon: 'ti-view-list'
      },
      {
        state: 'dashboard',
        short_label: 'B',
        name: 'Dashboard',
        type: 'sub',
        icon: 'ti-bar-chart',
        children: [
          {
            state: 'map-dashboard',
            name: 'Map Dashboard'
          },
          {
            state: 'beneficiary-dashboard',
            name: 'Beneficiary Dashboard'
          },
          {
            state: 'saathi-dashboard',
            name: 'Saathi Dashboard'
          }
        ]
      },
      {
        state: 'manage-users',
        short_label: 'B',
        name: 'Manage User',
        type: 'sub',
        icon: 'ti-user',
        children: [
          {
            state: 'state-anchor-list',
            name: 'State Anchor List'
          },
          {
            state: 'partner-list',
            name: 'Partner List'
          },
          {
            state: 'state-coordinator-list',
            name: 'State Coordinator List'
          },
          {
            state: 'district-coordinator-list',
            name: 'District Coordinator List'
          },
          {
            state: 'block-coordinator-list',
            name: 'Block Coordinator List'
          },
          {
            state: 'saathi-list',
            name: 'Saathi List'
          }
        ]
      }, {
            state: 'beneficiary-profile',
            short_label: 'D',
            name: 'Beneficiary Profile',
            type: 'link',
            icon: 'ti-user'
      }, {
            state: 'manage-form',
            short_label: 'D',
            name: 'Manage Forms',
            type: 'link',
            icon: 'ti-receipt'
      },
    ],
  },
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }
}
