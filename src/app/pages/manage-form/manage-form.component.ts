import { Component, OnInit } from '@angular/core';
import { ManageFormService } from '../../services/manage-form/manage-form.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-manage-form',
  templateUrl: './manage-form.component.html',
  styleUrls: ['./manage-form.component.css']
})
export class ManageFormComponent implements OnInit {
  parentCards: any = [];
  constructor(private manageFormServices: ManageFormService, private router: Router) { }
    ngOnInit() {
        this.getFormData();
    }
    /* ------------------------------------------------------
            START: Fetching data from service
    --------------------------------------------------------*/
    getFormData() {
      const scope = this;
      // get Card data
      this.manageFormServices.getCardsData().subscribe((cardResponse) => {

          // get manage form data
          const formsGroup = cardResponse.data;
          this.manageFormServices.getManageForms().subscribe((manageFormResponse) => {
            const formList = manageFormResponse.data;
            formsGroup.forEach(function(fg){
              fg['formCollection'] = [];
              formList.forEach(function(f){
                  if (fg.forms.indexOf(f.formId) > -1) {
                      fg.formCollection.push(f);
                  }
              });
              scope.parentCards.push(fg);
          });
          }, (error) => {
            console.log('Error:', error);
          });
          console.log("scope.parentCards", scope.parentCards);
          // End ..

      }, (error) => {
          console.log('Error', error);
      }); // End ..

    }
    /* ------------------------------------------------------
                              END
    --------------------------------------------------------*/
}
