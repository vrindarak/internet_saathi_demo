
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageFormComponent } from './manage-form.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';

export const BeneficiaryProfileRoutes: Routes = [
  {
    path: '',
    component: ManageFormComponent,
    data: {
      breadcrumb: 'Manage Form',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(BeneficiaryProfileRoutes),
    SharedModule,
    ChartModule
  ],
  declarations: [ManageFormComponent]
})
export class ManageFormModule { }
