
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StateCoordinatorListComponent } from './state-coordinator-list.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';

export const StateCoordinatorListRoutes: Routes = [
  {
    path: '',
    component: StateCoordinatorListComponent,
    data: {
      breadcrumb: 'Default',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(StateCoordinatorListRoutes),
    SharedModule,
    ChartModule
  ],
  declarations: [StateCoordinatorListComponent]
})
export class StateCoordinatorListModule { }
