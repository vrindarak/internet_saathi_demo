import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StateCoordinatorListComponent } from './state-coordinator-list.component';

describe('StateCoordinatorListComponent', () => {
  let component: StateCoordinatorListComponent;
  let fixture: ComponentFixture<StateCoordinatorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StateCoordinatorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateCoordinatorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
