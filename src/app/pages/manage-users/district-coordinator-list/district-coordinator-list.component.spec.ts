import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistrictCoordinatorListComponent } from './district-coordinator-list.component';

describe('DistrictCoordinatorListComponent', () => {
  let component: DistrictCoordinatorListComponent;
  let fixture: ComponentFixture<DistrictCoordinatorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistrictCoordinatorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistrictCoordinatorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
