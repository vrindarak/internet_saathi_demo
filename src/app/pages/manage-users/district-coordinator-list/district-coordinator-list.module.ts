
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DistrictCoordinatorListComponent } from './district-coordinator-list.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';

export const DistrictCoordinatorListRoutes: Routes = [
  {
    path: '',
    component: DistrictCoordinatorListComponent,
    data: {
      breadcrumb: 'Default',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DistrictCoordinatorListRoutes),
    SharedModule,
    ChartModule
  ],
  declarations: [DistrictCoordinatorListComponent]
})
export class DistrictCoordinatorListModule { }
