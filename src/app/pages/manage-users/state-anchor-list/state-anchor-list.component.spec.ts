import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StateAnchorListComponent } from './state-anchor-list.component';

describe('StateAnchorListComponent', () => {
  let component: StateAnchorListComponent;
  let fixture: ComponentFixture<StateAnchorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StateAnchorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateAnchorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
