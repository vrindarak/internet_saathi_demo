import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaathiListComponent } from './saathi-list.component';

describe('SaathiListComponent', () => {
  let component: SaathiListComponent;
  let fixture: ComponentFixture<SaathiListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaathiListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaathiListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
