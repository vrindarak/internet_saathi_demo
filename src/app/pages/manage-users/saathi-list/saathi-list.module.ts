
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaathiListComponent } from './saathi-list.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';

export const SaathiCoordinatorListRoutes: Routes = [
  {
    path: '',
    component: SaathiListComponent,
    data: {
      breadcrumb: 'Default',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SaathiCoordinatorListRoutes),
    SharedModule,
    ChartModule
  ],
  declarations: [SaathiListComponent]
})
export class SaathiListModule { }
