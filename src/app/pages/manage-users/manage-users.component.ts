import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-manage-users',
  template: '<router-outlet></router-outlet>'
  // '<router-outlet><app-spinner></app-spinner></router-outlet>'
})
export class ManageUsersComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
