
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageUsersComponent } from './manage-users.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
// import { ParnerListComponent } from './parner-list/parner-list.component';
// import { StateCoordinatorListComponent } from './state-coordinator-list/state-coordinator-list.component';
// import { SaathiListComponent } from './saathi-list/saathi-list.component';
// import { PartnerListComponent } from './partner-list/partner-list.component';
// import { StateAnchorListComponent } from './state-anchor-list/state-anchor-list.component';

export const ManageUsersRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Manage Users',
      status: false
    },
    children: [
      {
        path: 'state-anchor-list',
        loadChildren: './state-anchor-list/state-anchor-list.module#StateAnchorListModule'
      }, {
        path: 'partner-list',
        loadChildren: './partner-list/partner-list.module#PartnerListModule'
      }, {
        path: 'state-coordinator-list',
        loadChildren: './state-coordinator-list/state-coordinator-list.module#StateCoordinatorListModule'
      }, {
        path: 'district-coordinator-list',
        loadChildren: './district-coordinator-list/district-coordinator-list.module#DistrictCoordinatorListModule'
      }, {
        path: 'block-coordinator-list',
        loadChildren: './block-coordinator-list/block-coordinator-list.module#BlockCoordinatorListModule'
      }, {
        path: 'saathi-list',
        loadChildren: './saathi-list/saathi-list.module#SaathiListModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ManageUsersRoutes),
    SharedModule
  ],
  declarations: [ManageUsersComponent]
})
export class ManageUsersModule { }
