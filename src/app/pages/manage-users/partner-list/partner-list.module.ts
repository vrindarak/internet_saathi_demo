
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartnerListComponent } from './partner-list.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';

export const StateAnchorListRoutes: Routes = [
  {
    path: '',
    component: PartnerListComponent,
    data: {
      breadcrumb: 'Default',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(StateAnchorListRoutes),
    SharedModule,
    ChartModule
  ],
  declarations: [PartnerListComponent]
})
export class PartnerListModule { }
