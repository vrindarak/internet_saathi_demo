import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockCoordinatorListComponent } from './block-coordinator-list.component';

describe('BlockCoordinatorListComponent', () => {
  let component: BlockCoordinatorListComponent;
  let fixture: ComponentFixture<BlockCoordinatorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockCoordinatorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockCoordinatorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
