
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlockCoordinatorListComponent } from './block-coordinator-list.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';

export const BlockCoordinatorListRoutes: Routes = [
  {
    path: '',
    component: BlockCoordinatorListComponent,
    data: {
      breadcrumb: 'Default',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(BlockCoordinatorListRoutes),
    SharedModule,
    ChartModule
  ],
  declarations: [BlockCoordinatorListComponent]
})
export class BlockCoordinatorListModule { }
