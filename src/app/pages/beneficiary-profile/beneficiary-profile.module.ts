
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BeneficiaryProfileComponent } from './beneficiary-profile.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';

export const BeneficiaryProfileRoutes: Routes = [
  {
    path: '',
    component: BeneficiaryProfileComponent,
    data: {
      breadcrumb: 'Default',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(BeneficiaryProfileRoutes),
    SharedModule,
    ChartModule
  ],
  declarations: [BeneficiaryProfileComponent]
})
export class BeneficiaryProfileModule { }
