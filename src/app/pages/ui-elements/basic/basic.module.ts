import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicComponent } from './basic.component';
import {RouterModule, Routes} from '@angular/router';
// import {SharedModule} from "../../../shared/shared.module";
import { SaathiDasboardComponent } from './saathi-dasboard/saathi-dasboard.component';

export const BasicRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Basic Components',
      status: false
    },
    children: [
      {
        path: 'mapdashboard',
        loadChildren: './button/button.module#ButtonModule'
      }, {
        path: 'beneficiarydasboard',
        loadChildren: './typography/typography.module#TypographyModule'
      },
      {
        path: 'saathidashboard',
        loadChildren: './saathi-dasboard/saathi-dasboard.module#SaathiDasboardModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(BasicRoutes),
    // SharedModule
  ],
  declarations: [BasicComponent, SaathiDasboardComponent]
})
export class BasicModule { }
