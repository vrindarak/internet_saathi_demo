import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaathiDasboardComponent } from './saathi-dasboard.component';

describe('SaathiDasboardComponent', () => {
  let component: SaathiDasboardComponent;
  let fixture: ComponentFixture<SaathiDasboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaathiDasboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaathiDasboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
