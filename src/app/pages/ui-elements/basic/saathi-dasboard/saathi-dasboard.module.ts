import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaathiDasboardComponent } from './saathi-dasboard.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../../shared/shared.module';

export const SaathiDasboardRoutes: Routes = [
  {
    path: '',
    component: SaathiDasboardComponent,
    data: {
      breadcrumb: 'Typography',
      icon: 'icofont-layout bg-c-blue',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SaathiDasboardRoutes),
    SharedModule
  ],
  declarations: [SaathiDasboardComponent]
})
export class SaathiDasboardModule { }
