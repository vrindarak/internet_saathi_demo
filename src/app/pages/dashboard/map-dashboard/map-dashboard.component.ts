import { Component, OnInit } from '@angular/core';
import { MapDashboardService } from '../../../services/map-dashboard.service';
import { Router } from '@angular/router';
import { state } from '@angular/animations';

@Component({
  selector: 'app-map-dashboard',
  templateUrl: './map-dashboard.component.html',
  styleUrls: ['./map-dashboard.component.css']
})
export class MapDashboardComponent implements OnInit {

  constructor(private mapServices: MapDashboardService, private router: Router) { }
  countingData;
  partner;
  state;
  district;
  village;

  ngOnInit() {
    this.getMapCountData();
  }

  getMapCountData() { // Get Partners,State,District and village count..
    this.mapServices.getCounts().subscribe((data) => {
      this.partner = data.partner;
      this.state = data.state;
      this.district = data.district;
      this.village = data.village;
    }, (error) => {
      console.log(error);
    });

  }
}
