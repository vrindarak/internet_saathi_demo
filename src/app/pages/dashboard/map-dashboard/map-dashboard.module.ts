
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapDashboardComponent } from './map-dashboard.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';

export const MapDashboardDefaultRoutes: Routes = [
  {
    path: '',
    component: MapDashboardComponent,
    data: {
      breadcrumb: 'Map Dashboard',
      icon: 'icofont-home bg-c-blue',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MapDashboardDefaultRoutes),
    SharedModule,
    ChartModule
  ],
  declarations: [MapDashboardComponent]
})
export class MapDashboardModule { }
