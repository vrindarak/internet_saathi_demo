
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';

export const DashboardRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Dashboard',
      status: false
    },
    children: [
      {
        path: 'map-dashboard',
        loadChildren: './map-dashboard/map-dashboard.module#MapDashboardModule'
      }, {
        path: 'beneficiary-dashboard',
        loadChildren: './beneficiary-dashboard/beneficiary-dashboard.module#BeneficiaryDashboardModule'
      },
      {
        path: 'saathi-dashboard',
        loadChildren: './saathi-dashboard/saathi-dashboard.module#SaathiDashboardModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardRoutes),
    SharedModule
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
