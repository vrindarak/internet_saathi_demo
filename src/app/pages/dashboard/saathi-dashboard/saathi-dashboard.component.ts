import { Component, OnInit,  ViewChild, ElementRef} from '@angular/core';
import { SaathiDashboardService } from '../../../services/saathi-dashboard.service';
import { Router } from '@angular/router';
import { state } from '@angular/animations';
import {chart} from 'highcharts';

@Component({
  selector: 'app-saathi-dashboard',
  templateUrl: './saathi-dashboard.component.html',
  styleUrls: ['./saathi-dashboard.component.css']
})
export class SaathiDashboardComponent implements OnInit {
  @ViewChild('saathiAgeDistributionGraph') saathiAgeDistributionGraph: ElementRef; // Imp for Highchart graph ..
  @ViewChild('saathiEducationDistributionGraph') saathiEducationDistributionGraph: ElementRef; // Imp for Highchart graph ..
  @ViewChild('saathiOccupationGraph') saathiOccupationGraph: ElementRef; // Imp for Highchart graph ..
  totalSaathi: number;
  completedProfile: number;
  aadhaarSaathi: number;
  atmSaathi: number;
  ageDisValue: any = [];
  ageDisKey: any = [];
  eduDisValue: any = [];
  eduDisKey: any = [];
  occupValue: any = [];
  occupKey: any = [];
  chart: Highcharts.ChartObject; // Imp for Highchart graph ..
  constructor(private saathiServices: SaathiDashboardService, private router: Router) { }
  ngOnInit() {
      this.getSaathiData();
  }

  /* ------------------------------------------------------
            START: Fetching data from service
  --------------------------------------------------------*/


  getSaathiData() {
    // Count for Cards...
    this.saathiServices.getCounts().subscribe((res) => {
      const data = res.data;
      this.totalSaathi = data.saathiCount;
      this.completedProfile = data.profileComplete;
      this.aadhaarSaathi = data.haveAadhaarSaathiCount;
      this.atmSaathi = data.haveAtmSaathiCount;
      console.log('Data', data);
    }, (error) => {
      console.log('Error: ', error);
    }); // End..

    // Data for Saathi Age Distribution..
    this.saathiServices.getSaathiAgeDistributionData().subscribe((res) => {
      const saathiAgeData = res.data;
      this.ageDisValue = Object.values(saathiAgeData);
      this.ageDisKey = Object.keys(saathiAgeData);
      this.getSaathiAgeDistributionChart(this.ageDisValue);
    }, (error) => {
      console.log('Error:', error);
    }); // End..

    // Data for Saathi Education Distribution..
    this.saathiServices.getSaathiEduDistributionData().subscribe((res) => {
      const saathiEduData = res.data;
      this.eduDisValue = Object.values(saathiEduData);
      this.eduDisKey = Object.keys(saathiEduData);
      this.getSaathiEduDistributionChart(this.eduDisValue);
    }, (error) => {
      console.log('Error:', error);
    }); // End..

    // Data for Saathi Occupation Chart..
    this.saathiServices.getSaathiOccupationChartData().subscribe((res) => {
      const saathiOccupationData = res.data;
      this.occupValue = Object.values(saathiOccupationData);
      this.occupKey = Object.keys(saathiOccupationData);
      this.getSaathiOccupationChart(this.occupValue);
    }, (error) => {
      console.log('Error:', error);
    }); // End..


  }// End...

  /* ------------------------------------------------------
                            END
  --------------------------------------------------------*/

  /* ------------------------------------------------------
                            START: HIGHCHART
  --------------------------------------------------------*/
  getSaathiAgeDistributionChart(val) {
        const Options: Highcharts.Options =  {
          chart: {
            type: 'column'
        },
        title: {
            text: ''
        },

        xAxis: {
            categories: this.ageDisKey,
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Saathi Count'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    // color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            enabled: false
        },

        credits: {
            enabled: false
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">Total Saathis</span>: <b>{point.y}</b><br/>',
            shared: true,
            backgroundColor: 'white'
        },
        plotOptions: {
            column: {

                dataLabels: {
                    enabled: true,
                    // color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black'
                }
            }
        },
        series: [{
            data: val,
            color: '#009B4A'
        }],

        };
        this.chart = chart(this.saathiAgeDistributionGraph.nativeElement, Options);
  }

  getSaathiEduDistributionChart(val) {
      const Options: Highcharts.Options =  {
        chart: {
          type: 'column'
        },
        title: {
            text: ''
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: this.eduDisKey,
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Saathi Count'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    // color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">Total Saathis</span>: <b>{point.y}</b><br/>',
            shared: true,
            backgroundColor: 'white'
        },
        plotOptions: {
            column: {

                dataLabels: {
                    enabled: true,
                    // color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black'
                }
            }
        },
        series: [{
            data: val,
            color: '#008CDB'
        }],

      };
      this.chart = chart(this.saathiEducationDistributionGraph.nativeElement, Options);
  }

  getSaathiOccupationChart(val) {
    const Options: Highcharts.Options =  {
      chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    legend: {
        enabled: false
    },
    xAxis: {
        categories: this.occupKey,
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Saathi Count'
        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                // color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            }
        }
    },
    credits: {
        enabled: false
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">Total Saathis</span>: <b>{point.y}</b><br/>',
        shared: true,
        backgroundColor: 'white'
    },
    plotOptions: {
        column: {

            dataLabels: {
                enabled: true,
                // color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black'
            }
        }
    },
    series: [{
        data: val,
        color: '#e65100'
    }],

    };
    this.chart = chart(this.saathiOccupationGraph.nativeElement, Options);
}
  /* ------------------------------------------------------
                            END
    --------------------------------------------------------*/
}
