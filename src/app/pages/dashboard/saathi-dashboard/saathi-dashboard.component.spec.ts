import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaathiDashboardComponent } from './saathi-dashboard.component';

describe('SaathiDashboardComponent', () => {
  let component: SaathiDashboardComponent;
  let fixture: ComponentFixture<SaathiDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaathiDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaathiDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
