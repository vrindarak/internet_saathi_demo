
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaathiDashboardComponent } from './saathi-dashboard.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
// import {ChartModule} from 'angular2-chartjs';

export const SaathiDashboardRoutes: Routes = [
  {
    path: '',
    component: SaathiDashboardComponent,
    data: {
      breadcrumb: 'Saathi Dashboard',
      icon: 'icofont-home bg-c-blue',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SaathiDashboardRoutes),
    SharedModule,
    // ChartModule
  ],
  declarations: [SaathiDashboardComponent]
})
export class SaathiDashboardModule { }
