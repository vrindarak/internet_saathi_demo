
import { Component, OnInit, ViewChild, ElementRef} from '@angular/core'; // Imp for Highchart graph ..
import { BeneficiaryDashboardService } from '../../../services/beneficiary-dashboard.service';
import { Router } from '@angular/router';
import { state } from '@angular/animations';
import {chart} from 'highcharts';
// import { Chart } from 'angular-highcharts';
@Component({
  selector: 'app-beneficiary-dashboard',
  templateUrl: './beneficiary-dashboard.component.html',
  styleUrls: ['./beneficiary-dashboard.component.css']
})
export class BeneficiaryDashboardComponent implements OnInit {
    @ViewChild('responseofDigitalLiteracyGraph') responseofDigitalLiteracyGraph: ElementRef; // Imp for Highchart graph ..
    @ViewChild('individualsPartOfShgGraph') individualsPartOfShgGraph: ElementRef;
    @ViewChild('individualAgeDistributionGraph') individualAgeDistributionGraph: ElementRef;
    @ViewChild('digitalLitracyScoreCardGraph') digitalLitracyScoreCardGraph: ElementRef;
    active;
    total = 200;
    yes: any = [];
    no: any = [];
    label = [];
    male: number;
    female: number;
    beneficiary: number;
    aadhaar: string;
    shg_yes: number;
    shg_no: number;
    value: any = [];
    key: any = [];
    digitalLitVal: any = [];
    digitalLitKey: any = [];

    chart: Highcharts.ChartObject; // Imp for Highchart graph ..
    constructor(private benefServices: BeneficiaryDashboardService, private router: Router) { }
    ngOnInit() {
        this.getBenefCountData();
    }

  /* ------------------------------------------------------
            START: Fetching data from service
    --------------------------------------------------------*/
    getBenefCountData() { // ..
            const scope = this;

            this.benefServices.getCounts().subscribe((res) => {
                const data = res.data;
                this.active = res.activeSaathiCount;
                data.forEach(function (element) {
                    if (element.order === '3') {
                        scope.male = element.count;
                        scope.female = element.elseCount; // '123344555667783333';
                        scope.beneficiary = element.total;
                    }
                    if (element.order === '8') {
                        scope.aadhaar = (element.count / element.total * 100).toFixed(2);
                    }
                    if (element.order === '10') {
                        scope.shg_yes = element.count;
                        scope.shg_no = element.elseCount;
                    }
                    if (element.order !== '3' && element.order !== '8' && element.order !== '10') {
                        scope.yes.push(element.count);
                        scope.no.push(element.elseCount);
                        scope.label.push(element.label);
                        scope.total = element.total;
                    }
                });
                this.responseofDigitalLiteracyChart(scope.yes, scope.no);
                this.individualsPartOfShgChart(scope.shg_yes, scope.shg_no);
            }, (error) => {
                console.log('Error', error);
            });

            // Individual Age distribution data ..
            this.benefServices.getAgeDistributionData().subscribe((res) => {
                const distData = res.data;
                scope.value = Object.values(distData);
                scope.key = Object.keys(distData);
                this.individualAgeDistributionChart(scope.value);
            }, (error) => {
                console.log('Error:', error);
            }); // End..

            // Digital Litracy Score Card data ..
            this.benefServices.digitalLitracyScoreCard().subscribe((res) => {
                const digtalLitData = res.data;
                scope.digitalLitVal = Object.values(digtalLitData);
                scope.digitalLitKey = Object.keys(digtalLitData);
                this.digitalLitracyScoreCardChart(scope.digitalLitVal);
            }, (error) => {
                console.log('Error:', error);
            }); // End..
    }

    /* ------------------------------------------------------
                            END
    --------------------------------------------------------*/

    /* ------------------------------------------------------
                            START: HIGHCHART
    --------------------------------------------------------*/
    individualAgeDistributionChart(value) {
        const Options: Highcharts.Options =  {
            chart: {
                type: 'column'
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Individual age distribution'
            },

            xAxis: {
                categories: this.key,
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Individual Count'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        // color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },

            tooltip: {
                pointFormat: '<span style="color:{series.color}">Total Individuals</span>: <b>{point.y}</b><br/>',
                shared: true,
                backgroundColor: 'white'
            },
            plotOptions: {
                column: {

                    dataLabels: {
                        enabled: true,
                        // color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black'
                    }
                }
            },
            series: [{
                data: value,
                color: '#F25E00'
            }],
        };
        this.chart = chart(this.individualAgeDistributionGraph.nativeElement, Options);
    }

    individualsPartOfShgChart(shg_yes, shg_no) {
        const Options: Highcharts.Options =  {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '<b>{point.y}</b>',
                backgroundColor: 'white'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            // color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                // colorByPoint: true,
                data: [{
                    name: 'Yes',
                    y: shg_yes,
                    color: '#008CDB'
                }, {
                    name: 'No',
                    y: shg_no,
                    color: '#009B4A'
                }]
            }],
        };
        this.chart = chart(this.individualsPartOfShgGraph.nativeElement, Options);
    }

    responseofDigitalLiteracyChart(yes, no) {
        const Options: Highcharts.Options =  {
                chart: {
                    type: 'column'
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: this.label
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Yes / No Responses'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            // color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                            color: '#008CDB',
                        }
                    }
                },
                tooltip: {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> {point.percentage:.1f} %<br/>',
                    shared: true,
                    backgroundColor: 'white'
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Yes',
                    data: yes,
                    color: '#008CDB'
                }, {
                    name: 'No',
                    data: no,
                    color: '#F25E00'
                }],

            };
            this.chart = chart(this.responseofDigitalLiteracyGraph.nativeElement, Options);
    }

    digitalLitracyScoreCardChart(val) {
        const Options: Highcharts.Options =  {
            chart: {
                type: 'column'
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },

            xAxis: {
                categories: this.digitalLitKey,
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Count of Beneficiaries'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        // color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },

            tooltip: {
                pointFormat: '<span style="color:{series.color}">Count of Beneficiaries</span>: <b>{point.y}</b><br/>',
                shared: true,
                backgroundColor: 'white'
            },
            plotOptions: {
                column: {

                    dataLabels: {
                        enabled: true,
                        // color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black'
                    }
                }
            },
            series: [{
                data: val,
                color: '#DAB400'
            }],

            };
            this.chart = chart(this.digitalLitracyScoreCardGraph.nativeElement, Options);
    }
    /* ------------------------------------------------------
                            END
    --------------------------------------------------------*/
}

