import { Component , OnInit, ViewContainerRef} from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import 'rxjs/add/operator/toPromise';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { FormsModule } from '@angular/forms';
// import { User } from '../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  data: any;
  object;
  loader = false;
  chosenOption;
  userList = [
      { 'value': 'admin', 'label': 'Admin' },
      { 'value': 'misHead', 'label': 'MIS Head' },
      { 'value': 'stateAnchor', 'label': 'State Anchor' },
      { 'value': 'partner', 'label': 'Partner' },
      { 'value': 'stateCoordinator', 'label': 'State Coordinator' },
      { 'value': 'districtCoordinator', 'label': 'District Coordinator' },
      { 'value': 'clientPartner', 'label': 'Client Partner' },
  ];
  constructor(private userService: UserService, private router: Router, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.object = this.userList[0];
    const token = localStorage.getItem('token');
    if (token) {
      this.router.navigate(['/dashboard/map-dashboard']);
    }
  }
  changedOption() {
    console.log(this.object);
    this.object = this.object;
  }
  async login(form) {
      const loginData = form.value;
      try {
        const response = await this.userService.login(loginData).toPromise();
        console.log('response', response);
        if (response['success'] === true) {
            const data = response;
            const usrData = response['data'];
            this.toastr.success('Success !', 'Success', {});
            sessionStorage.setItem('token', data['token']);
            sessionStorage.setItem('userData', JSON.stringify(data['data']));
            sessionStorage.setItem('userType', loginData.userType);
            sessionStorage.setItem('user', usrData['email']);
            sessionStorage.setItem('userName', usrData['name']);
            this.router.navigate(['/dashboard/map-dashboard']);
        }
      } catch (error) {
        this.toastr.warning('Check your Email or Password !', 'Incorrect  Credentials', {});
      }
  }
}

