
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartnerOrganizationListComponent } from './partner-organization-list.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';

export const PartnerOrganizationListRoutes: Routes = [
  {
    path: '',
    component: PartnerOrganizationListComponent,
    data: {
      breadcrumb: 'Default',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PartnerOrganizationListRoutes),
    SharedModule,
    ChartModule
  ],
  declarations: [PartnerOrganizationListComponent]
})
export class PartnerOrganizationListModule { }
