import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerOrganizationListComponent } from './partner-organization-list.component';

describe('PartnerOrganizationListComponent', () => {
  let component: PartnerOrganizationListComponent;
  let fixture: ComponentFixture<PartnerOrganizationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerOrganizationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerOrganizationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
