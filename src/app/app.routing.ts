import {Routes} from '@angular/router';
import {AdminComponent} from './layout/admin/admin.component';
import {AuthComponent} from './layout/auth/auth.component';
import { AuthGuard } from './pages/guard/auth.guard';

export const AppRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate : [ AuthGuard ],
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }, {
        path: 'partner-organization-list',
        loadChildren: './pages/partner-organization-list/partner-organization-list.module#PartnerOrganizationListModule'
      }, {
        path: 'dashboard',
        loadChildren: './pages/dashboard/dashboard.module#DashboardModule'
      }, {
        path: 'manage-users',
        loadChildren: './pages/manage-users/manage-users.module#ManageUsersModule'
      }, {
        path: 'beneficiary-profile',
        loadChildren: './pages/beneficiary-profile/beneficiary-profile.module#BeneficiaryProfileModule'
      }, {
        path: 'manage-form',
        loadChildren: './pages/manage-form/manage-form.module#ManageFormModule'
      }
    ]
  }, {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'login',
        loadChildren: './pages/login/login.module#LoginModule'
      }
    ]
  }
];
