import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutes } from './app.routing';
import { ToastModule } from 'ng2-toastr/ng2-toastr';

// Services
import { UserService } from './services/user.service';
import { MapDashboardService } from './services/map-dashboard.service';
import { BeneficiaryDashboardService } from './services/beneficiary-dashboard.service';
import { SaathiDashboardService } from './services/saathi-dashboard.service';
import { ManageFormService } from './services/manage-form/manage-form.service';

// Guard
import { AuthGuard } from './pages/guard/auth.guard';
import { SharedModule } from './shared/shared.module';

// Components
import { AppComponent } from './app.component';
import { AdminComponent } from './layout/admin/admin.component';
import { BreadcrumbsComponent } from './layout/admin/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from './layout/admin/title/title.component';
import { AuthComponent } from './layout/auth/auth.component';

import { ClickOutsideModule } from 'ng-click-outside';


@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    BreadcrumbsComponent,
    TitleComponent,
    AuthComponent,
  ],

  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(AppRoutes),
    ClickOutsideModule,
    SharedModule,
    ToastModule.forRoot(),
  ],

  providers: [
                UserService, MapDashboardService,
                AuthGuard , BeneficiaryDashboardService,
                SaathiDashboardService, ManageFormService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
