import { TestBed, inject } from '@angular/core/testing';

import { MapDashboardService } from './map-dashboard.service';

describe('MapDashboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MapDashboardService]
    });
  });

  it('should be created', inject([MapDashboardService], (service: MapDashboardService) => {
    expect(service).toBeTruthy();
  }));
});
