import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
@Injectable()
export class ManageFormService {
  base_uri: string = environment.base_uri;
  private headers;
  private options;
  mapData;
  results;
  constructor(private httpClient: HttpClient, private http: Http, private router: Router) {}
  setHeaders() { // Setting headers..
    this.headers = new Headers({'Content-Type' : 'application/json', 'x-access-token' : this.getToken()});
    this.options = new RequestOptions({headers : this.headers});
    // console.log('this.options', this.headers);
    return this.options;

  }

  getToken() { // Get Token..
    return sessionStorage.getItem('token');
  }

  getCardsData() {
    return this.http.get(this.base_uri + 'form-group', this.setHeaders()).map((response: Response) => response.json())
    .catch(this.handleError);
  }

  getManageForms() {
    return this.http.get(this.base_uri + 'dashboard/manageform', this.setHeaders()).map((response: Response) => response.json())
    .catch(this.handleError);
  }

  handleError(error: Response) {
    return Observable.throw(error || 'SERVER ERROR');
  }

}
