import { TestBed, inject } from '@angular/core/testing';

import { ManageFormService } from './manage-form.service';

describe('ManageFormService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ManageFormService]
    });
  });

  it('should be created', inject([ManageFormService], (service: ManageFormService) => {
    expect(service).toBeTruthy();
  }));
});
