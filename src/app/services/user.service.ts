import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
// import { User } from 'app/models/user';

@Injectable()
export class UserService {
  base_uri: string = environment.base_uri;
  private headers;
  private options;
  private isUserLoggedIn;
  private username;

  constructor(private httpClient: HttpClient, private http: Http, private router: Router) {
    this.isUserLoggedIn = false;
  }

  login(data) { // Login Function Call..
    return this.httpClient.post(this.base_uri + 'login', data);
  }

  setHeaders() {  // Setting headers..
    this.headers = new Headers({'Content-Type' : 'application/json', 'token' : this.getToken()});
    this.options = new RequestOptions({headers : this.headers});
    return this.options;
  }

  getToken() { // Get Token..
    return sessionStorage.getItem('token');
  }

  setUserLoggedIn() { // Change user login status..
    this.isUserLoggedIn = true;
  }

  getUserLoggedIn() { // Check that is user logged in or not..
    const token = sessionStorage.getItem('token');
    if (token) {
      return true;
    }
    return false;
  }
}
