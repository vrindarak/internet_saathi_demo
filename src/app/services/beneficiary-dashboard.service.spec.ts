import { TestBed, inject } from '@angular/core/testing';

import { BeneficiaryDashboardService } from './beneficiary-dashboard.service';

describe('BeneficiaryDashboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BeneficiaryDashboardService]
    });
  });

  it('should be created', inject([BeneficiaryDashboardService], (service: BeneficiaryDashboardService) => {
    expect(service).toBeTruthy();
  }));
});
