import { TestBed, inject } from '@angular/core/testing';

import { SaathiDashboardService } from './saathi-dashboard.service';

describe('SaathiDashboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SaathiDashboardService]
    });
  });

  it('should be created', inject([SaathiDashboardService], (service: SaathiDashboardService) => {
    expect(service).toBeTruthy();
  }));
});
